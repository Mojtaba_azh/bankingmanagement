<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
    Locale persianLocale = new Locale("fa", "IR");
    Locale englishLocale = new Locale("en", "US");
    ResourceBundle bundle = null;
    String selectedLanguage = (String) request.getSession().getAttribute("selectedLanguage");
    if (selectedLanguage.equals("Persian")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/fa/MessageBundle", persianLocale);
    } else if (selectedLanguage.equals("English")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/en/MessageBundle", englishLocale);
    }
    String textBoxLanguageClass;
    assert bundle != null;%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><%=bundle.getString("addLoan")%>
    </title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link rel="stylesheet" href="css/vertical-responsive-menu.min.css"/>
    <link rel="stylesheet" href="css/persianDatepicker-default.css"/>
    <script src="js/jquery-1.10.1.min.js"></script>
    <script src="js/persianDatepicker.js"></script>
    <script src="js/functions.js"></script>
</head>
<%
    if (selectedLanguage.equals("Persian")) {
        textBoxLanguageClass = "persianTextBox";
%>
<body style="direction: rtl">
    <%
} else {
        textBoxLanguageClass = "englishTextBox";
%>
<body style="direction: ltr">
<%
    }
%>
<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: #fbbc05;height: 60px;">
        <ul class="navbar-nav">
            <li><a href="<%=request.getContextPath()%>/home" class="nav-link"><%=bundle.getString("homeTitle")%>
            </a></li>
        </ul>
    </nav>
</header>
<br>
<%
    if (selectedLanguage.equals("Persian")) {
%>
<div class="container text-right">
        <%
    } else {
    %>
    <div class="container text-left">
        <%
            }
        %>
        <div class="card">
            <div class="card-body">
                <form action="add-grant" method="post" autocomplete="off">
                    <h2 class="text-center"><%=bundle.getString("addLoan")%>
                    </h2>
                    <br>
                    <div class="row">
                        <div class="col-md-8">
                            <fieldset class="form-group">
                                <label id="loanNameLabel"><%=bundle.getString("loanName")%>
                                </label>
                                <input id="loanNameInput" type="text" class="form-control <%=textBoxLanguageClass%>"
                                       name="loanName"
                                       title="<%=bundle.getString("loanName")%>"
                                       required="required"
                                       oninput="checkLoanInputs();this.setCustomValidity('')"
                                       <%
                               if (selectedLanguage.equals("Persian")) {
                           %>oninvalid="this.setCustomValidity(invalidInputFA(document.getElementById('loanNameLabel').innerHTML))"
                                    <%
                        } else {
                           %>
                                       oninvalid="this.setCustomValidity(invalidInputEN(document.getElementById('loanNameLabel').innerHTML))"
                                    <%
                        }
                           %>
                                >
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset class="form-group">
                                <label id="loanRateLabel"><%=bundle.getString("loanRate")%>
                                </label>
                                <input id="loanRateInput" type="text" class="form-control numberTextBox" name="loanRate"
                                       minlength="1" maxlength="3"
                                       title="<%=bundle.getString("loanRate")%>" required="required"
                                       oninput="checkLoanInputs();this.setCustomValidity('')"
                                    <%
                               if (selectedLanguage.equals("Persian")) {
                           %>
                                       oninvalid="this.setCustomValidity(invalidInputFA(document.getElementById('loanRateLabel').innerHTML))"
                                    <%
                                    } else {
                                       %>
                                       oninvalid="this.setCustomValidity(invalidInputEN(document.getElementById('loanRateLabel').innerHTML))"
                                    <%
                                    }
                                       %>
                                >
                            </fieldset>
                        </div>
                    </div>
                    <div id="loanGrantsDiv" style="display: none">
                        <button type="submit" class="btn btn-primary"><%=bundle.getString("addGrant")%>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
