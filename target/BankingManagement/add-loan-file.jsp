<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>
<%@ page import="model.entity.CustomerEntity" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    request.setCharacterEncoding("UTF-8");
    Locale persianLocale = new Locale("fa", "IR");
    Locale englishLocale = new Locale("en", "US");
    ResourceBundle bundle = null;
    String selectedLanguage = (String) request.getSession().getAttribute("selectedLanguage");
    if (selectedLanguage.equals("Persian")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/fa/MessageBundle", persianLocale);
    } else if (selectedLanguage.equals("English")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/en/MessageBundle", englishLocale);
    }
    Long customerId = null;
    String customerCodeValue = "";
    String customerNameValue = "";
    String customerFamilyValue = "";
    String loanId="";
    String loanName = "";
    String loanAmount = "";
    String loanDuration = "";
    CustomerEntity customer;
    assert bundle != null;
    String customerCodeLabel = bundle.getString("nationalCode");
    String customerNameLabel = bundle.getString("firstName");
    try {
        customer = (CustomerEntity) request.getAttribute("customer");
        if (customer != null) {
            customerCodeValue = customer.getCustomerCode();
            customerNameValue = customer.getCustomerName();
            customerId = customer.getCustomerId();
            if (customer.getLoanAmount() != null && customer.getLoanDuration() != null) {
                loanId = customer.getLoanByLoanId().getLoanId()+"";
                loanName = customer.getLoanByLoanId().getLoanName();
                loanAmount = customer.getLoanAmount().toString().split("\\.")[0];
                loanDuration = customer.getLoanDuration() + "";
            }
            if (!customer.getCustomerType()) {
                customerCodeLabel = bundle.getString("economicCode");
                customerNameLabel = bundle.getString("companyName");
            } else
                customerFamilyValue = customer.getCustomerFamily();
        }
    } catch (Exception e) {
        customer = null;
    }

%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><%=bundle.getString("addLoanFile")%>
    </title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link rel="stylesheet" href="css/vertical-responsive-menu.min.css"/>
    <link rel="stylesheet" href="css/persianDatepicker-default.css"/>
    <script src="js/jquery-1.10.1.min.js"></script>
    <script src="js/persianDatepicker.js"></script>
    <script src="js/functions.js"></script>
</head>
<%
    if (selectedLanguage.equals("Persian")) {
%>
<body style="direction: rtl">
    <%
} else {
%>
<body style="direction: ltr">
<%
    }
%>
<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: #34a853;height: 60px;">
        <ul class="navbar-nav">
            <li><a href="<%=request.getContextPath()%>/home" class="nav-link"><%=bundle.getString("homeTitle")%>
            </a></li>
        </ul>
    </nav>
</header>
<br>
<%
    if (selectedLanguage.equals("Persian")) {
%>
<div class="container text-right">
    <%
    } else {
    %>
    <div class="container text-left">
        <%
            }
        %>
        <div class="card">
            <div class="card-body">
                <h2 class="text-center"><%=bundle.getString("addLoanFile")%>
                </h2>
                <br>
                <form action="show-customer-by-id" method="post" autocomplete="off">
                    <div class="row">
                        <div class="col-md-8">
                            <fieldset class="form-group">
                                <label id="customerIdLabel"><%=bundle.getString("customerId")%>
                                </label>
                                <input id="customerIdInput" type="text" class="form-control numberTextBox" name="customerIdToFind"
                                       minlength="1"
                                       title="<%=bundle.getString("customerId")%>" required="required"
                                       oninput="this.setCustomValidity('')"
                                    <%
                               if (selectedLanguage.equals("Persian")) {
                           %>
                                       oninvalid="this.setCustomValidity(invalidInputFA(document.getElementById('customerIdLabel').innerHTML))"
                                    <%
                                    } else {
                                       %>
                                       oninvalid="this.setCustomValidity(invalidInputEN(document.getElementById('customerIdLabel').innerHTML))"
                                    <%
                                    }
                                       %>
                                >
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset class="form-group">
                                <label>-</label>
                                <br>
                                <button type="submit"
                                        class="btn btn-primary"><%=bundle.getString("showCustomerInformation")%>
                                </button>
                            </fieldset>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <%
            if (customer != null) {
        %>
        <div id="customerInformationDiv" style="display: block">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <fieldset class="form-group">
                                <label id="customerCodeLabel"><%=customerCodeLabel%>
                                </label>
                                <input id="customerCodeInput" type="text" class="form-control" name="customerCode"
                                       value="<%=customerCodeValue%>"
                                       title="<%=customerCodeLabel%>" readonly>
                            </fieldset>
                        </div>
                        <div class="col">
                            <fieldset class="form-group">
                                <label id="customerNameLabel"><%=customerNameLabel%>
                                </label>
                                <input id="customerNameInput" type="text" class="form-control" name="customerName"
                                       value="<%=customerNameValue%>"
                                       title="<%=customerNameLabel%>" readonly>
                            </fieldset>
                        </div>
                        <%
                            if (customer.getCustomerType()) {
                        %>
                        <div class="col">
                            <fieldset id="customerFamilyField" class="form-group">
                                <label id="customerFamilyLabel"><%=bundle.getString("lastName")%>
                                </label>
                                <input id="customerFamilyInput" type="text" class="form-control" name="customerFamily"
                                       value="<%=customerFamilyValue%>"
                                       title="<%=bundle.getString("lastName")%>" readonly>
                            </fieldset>
                        </div>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
        <div id="loanFileDiv" style="display: block">
            <div class="card">
                <div class="card-body">
                    <form action="insert-loan-file" method="post" autocomplete="off">
                        <div class="row">
                            <div class="col-md-5">
                                <fieldset class="form-group">
                                    <label for="loanName" id="loanNameLabel"><%=bundle.getString("loanName")%>
                                    </label>
                                    <select id="loanName" class="form-control" name="loanName"
                                            title="<%=bundle.getString("loanName")%>">
                                        <option value="<%=loanId%>" selected style="font-style: italic"><%=loanName%>
                                            <c:forEach var="loan" items="${loans}">
                                        <option value="${loan.loanId}">${loan.loanName}</option>
                                        </c:forEach>
                                    </select>
                                </fieldset>
                            </div>
                            <div class="col-md-5">
                                <fieldset class="form-group">
                                    <label id="loanAmountLabel"><%=bundle.getString("loanAmount")%>
                                    </label>
                                    <input id="loanAmountInput" type="text" class="form-control numberTextBox" name="loanAmount"
                                           value="<%=loanAmount%>"
                                           minlength="1" maxlength="17"
                                           title="<%=bundle.getString("loanAmount")%>" required="required"
                                           oninput="checkLoanInputs();this.setCustomValidity('')"
                                        <%
                               if (selectedLanguage.equals("Persian")) {
                           %>
                                           oninvalid="this.setCustomValidity(invalidInputFA(document.getElementById('loanAmountLabel').innerHTML))"
                                        <%
                                    } else {
                                       %>
                                           oninvalid="this.setCustomValidity(invalidInputEN(document.getElementById('loanAmountLabel').innerHTML))"
                                        <%
                                    }
                                       %>
                                    >
                                </fieldset>
                            </div>
                            <div class="col-md-2">
                                <fieldset class="form-group">
                                    <label id="loanDurationLabel"><%=bundle.getString("loanDuration")%>
                                    </label>
                                    <input id="loanDurationInput" type="text" class="form-control numberTextBox" name="loanDuration"
                                           value="<%=loanDuration%>"
                                           minlength="1" maxlength="7"
                                           title="<%=bundle.getString("loanDuration")%>" required="required"
                                           oninput="checkLoanInputs();this.setCustomValidity('')"
                                        <%
                               if (selectedLanguage.equals("Persian")) {
                           %>
                                           oninvalid="this.setCustomValidity(invalidInputFA(document.getElementById('loanDurationLabel').innerHTML))"
                                        <%
                                    } else {
                                       %>
                                           oninvalid="this.setCustomValidity(invalidInputEN(document.getElementById('loanDurationLabel').innerHTML))"
                                        <%
                                    }
                                       %>
                                    >
                                </fieldset>
                            </div>
                        </div>
                        <input type="hidden" name="customerId" value="<%=customerId%>">
                        <button type="submit" class="btn btn-primary"
                                onclick="this.submit"><%=bundle.getString("addLoanFile")%>
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <%
            }
        %>
    </div>
</div>
</body>
</html>
