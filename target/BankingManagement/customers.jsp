<%@ page import="utility.DateConverter" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    request.setCharacterEncoding("UTF-8");
    Locale persianLocale = new Locale("fa", "IR");
    Locale englishLocale = new Locale("en", "US");
    ResourceBundle bundle = null;
    String selectedLanguage = (String) request.getSession().getAttribute("selectedLanguage");
    if (selectedLanguage.equals("Persian")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/fa/MessageBundle", persianLocale);
    } else if (selectedLanguage.equals("English")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/en/MessageBundle", englishLocale);
    }
    boolean customerType = (boolean) request.getAttribute("customerType");
    assert bundle != null;%>
<html>
<head>
    <title><%=bundle.getString("customersList")%>
    </title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
</head>
<%
    if (selectedLanguage.equals("Persian")) {
%>
<body style="direction: rtl">
    <%
} else {
%>
<body style="direction: ltr">
<%
    }
%>
<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: darkorchid;height: 60px;">
        <ul class="navbar-nav">
            <li><a href="<%=request.getContextPath()%>/home" class="nav-link"><%=bundle.getString("homeTitle")%>
            </a></li>
            <li><a href="<%=request.getContextPath()%>/find-customer"
                   class="nav-link"><%=bundle.getString("findCustomers")%>
            </a></li>
        </ul>
    </nav>
</header>
<br>
<%
    if (selectedLanguage.equals("Persian")) {
%>
<div class="container text-right">
        <%
    } else {
    %>
    <div class="container text-left">
        <%
            }
        %>
        <h3 class="text-center"><%=bundle.getString("customersList")%>
        </h3>
        <hr>
        <table class="table table-bordered table-striped">
            <%
                if (customerType) {
            %>
            <thead class="thead-dark">
            <tr>
                <th><%=bundle.getString("customerId")%>
                </th>
                <th><%=bundle.getString("nationalCode")%>
                </th>
                <th><%=bundle.getString("firstName")%>
                </th>
                <th><%=bundle.getString("lastName")%>
                </th>
                <th><%=bundle.getString("fatherName")%>
                </th>
                <th><%=bundle.getString("birthDate")%>
                </th>
                <th colspan="2"><%=bundle.getString("actions")%>
                </th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="customer" items="${customers}">
                <tr>
                    <td><c:out value="${customer.customerId}"/></td>
                    <td><c:out value="${customer.customerCode}"/></td>
                    <td><c:out value="${customer.customerName}"/></td>
                    <td><c:out value="${customer.customerFamily}"/></td>
                    <td><c:out value="${customer.customerFather}"/></td>
                    <%
                        if (selectedLanguage.equals("Persian")) {
                    %>
                    <td><c:out value="${DateConverter.convertToJalali(customer.customerBirthdate)}"/></td>
                    <%
                    } else {
                    %>
                    <td><c:out value="${customer.customerBirthdate}"/></td>
                    <%
                        }
                    %>
                    <td style="white-space:nowrap;">
                        <form action="edit-customer" method="post">
                            <input type="hidden" name="id" value="<c:out value='${customer.customerId}' />">
                            <input type="hidden" name="type" value=real>
                            <input type="hidden" name="jalaliBirthDate"
                                   value="<c:out value='${DateConverter.convertToJalali(customer.customerBirthdate)}' />">
                            <button type="submit" class="btn btn-warning btn-sm"><%=bundle.getString("edit")%>
                            </button>
                        </form>
                    </td>
                    <td style="white-space:nowrap;">
                        <form action="delete-customer" method="post">
                            <input type="hidden" name="id" value="<c:out value='${customer.customerId}' />">
                            <button type="submit" class="btn btn-danger btn-sm"><%=bundle.getString("delete")%>
                            </button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
            <%
            } else {
            %>
            <thead class="thead-dark">
            <tr>
                <th><%=bundle.getString("customerId")%>
                </th>
                <th><%=bundle.getString("economicCode")%>
                </th>
                <th><%=bundle.getString("companyName")%>
                </th>
                <th><%=bundle.getString("registrationDate")%>
                </th>
                <th colspan="2"><%=bundle.getString("actions")%>
                </th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="customer" items="${customers}">
                <tr>
                    <td><c:out value="${customer.customerId}"/></td>
                    <td><c:out value="${customer.customerCode}"/></td>
                    <td><c:out value="${customer.customerName}"/></td>
                    <%
                        if (selectedLanguage.equals("Persian")) {
                    %>
                    <td><c:out value="${DateConverter.convertToJalali(customer.customerBirthdate)}"/></td>
                    <%
                    } else {
                    %>
                    <td><c:out value="${customer.customerBirthdate}"/></td>
                    <%
                        }
                    %>
                    <td style="white-space:nowrap;">
                        <form action="edit-customer" method="post">
                            <input type="hidden" name="id" value="<c:out value='${customer.customerId}' />">
                            <input type="hidden" name="type" value="legal">
                            <input type="hidden" name="jalaliBirthDate"
                                   value="<c:out value='${DateConverter.convertToJalali(customer.customerBirthdate)}' />">
                            <button type="submit" class="btn btn-warning btn-sm"><%=bundle.getString("edit")%>
                            </button>
                        </form>
                    </td>
                    <td style="white-space:nowrap;">
                        <form action="delete-customer" method="post">
                            <input type="hidden" name="id" value="<c:out value='${customer.customerId}' />">
                            <button type="submit" class="btn btn-danger btn-sm"><%=bundle.getString("delete")%>
                            </button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
            <%
                }
            %>
        </table>
    </div>
</body>
</html>
