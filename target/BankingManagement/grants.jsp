<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    request.setCharacterEncoding("UTF-8");
    Locale persianLocale = new Locale("fa", "IR");
    Locale englishLocale = new Locale("en", "US");
    ResourceBundle bundle = null;
    String selectedLanguage = (String) request.getSession().getAttribute("selectedLanguage");
    if (selectedLanguage.equals("Persian")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/fa/MessageBundle", persianLocale);
    } else if (selectedLanguage.equals("English")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/en/MessageBundle", englishLocale);
    }
    assert bundle != null;%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><%=bundle.getString("addGrant")%>
    </title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link rel="stylesheet" href="css/vertical-responsive-menu.min.css"/>
    <link rel="stylesheet" href="css/persianDatepicker-default.css"/>
    <script src="js/jquery-1.10.1.min.js"></script>
    <script src="js/persianDatepicker.js"></script>
    <script src="js/functions.js"></script>
</head>
<body>
<table class="table table-bordered table-striped table-sm">
    <thead class="thead-dark">
    <tr>
        <th><%=bundle.getString("grantedId")%>
        </th>
        <th><%=bundle.getString("grantedName")%>
        </th>
        <th><%=bundle.getString("minimumAmount")%>
        </th>
        <th><%=bundle.getString("maximumAmount")%>
        </th>
        <th><%=bundle.getString("minimumDuration")%>
        </th>
        <th><%=bundle.getString("maximumDuration")%>
        </th>
        <th><%=bundle.getString("actions")%>
        </th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="grant" items="${grants}">
        <tr>
            <td><c:out value="${grant.grantedId}"/></td>
            <td><c:out value="${grant.grantedName}"/></td>
            <td><c:out value="${grant.minimumAmount}"/></td>
            <td><c:out value="${grant.maximumAmount}"/></td>
            <td><c:out value="${grant.minimumDuration}"/></td>
            <td><c:out value="${grant.maximumDuration}"/></td>
            <td style="white-space:nowrap;">
                <form action="delete-grant" method="post"
                        <%
                            if (((List) request.getSession().getAttribute("grants")).size() == 1) {
                        %>
                      onsubmit="alert('<%=bundle.getString("deleteGrantError")%>');return false;"
                        <%
                            }
                        %>>
                    <input type="hidden" name="id" value="<c:out value='${grant.grantedId}' />">
                    <button type="submit" class="btn btn-danger btn-sm"><%=bundle.getString("delete")%>
                    </button>
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
