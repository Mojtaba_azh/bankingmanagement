package model.dao;

import model.entity.LoanEntity;
import utility.LoggingUtility;
import utility.HibernateUtility;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class LoanDAO {

    public void createLoan(LoanEntity loan) {
        Transaction transaction = null;
        try (Session session = HibernateUtility.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(loan);
            transaction.commit();
            LoggingUtility.writeLog("اطلاعات تسهیلات شماره " + loan.getLoanId() + " با موفقیت به پایگاه داده افزوده شد.");
        } catch (Exception e) {
            if (transaction != null) {
                LoggingUtility.writeLog("خطا در ایجاد تسهیلات جدید - در حال انجام عملیات بازگشت (rollback)");
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public LoanEntity readLoan(long id) {
        Transaction transaction = null;
        try (Session session = HibernateUtility.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            LoanEntity loan = session.byId(LoanEntity.class).getReference(id);
            transaction.commit();
            LoggingUtility.writeLog("اطلاعات تسهیلات شماره " + loan.getLoanId() + " با موفقیت از پایگاه داده استخراج شد.");
            return loan;
        } catch (Exception e) {
            if (transaction != null) {
                LoggingUtility.writeLog("خطا در استخراج اطلاعات تسهیلات - در حال انجام عملیات بازگشت (rollback)");
                transaction.rollback();
            }
            e.printStackTrace();
            return null;
        }
    }

    public List<LoanEntity> readLoans() {
        Session session = HibernateUtility.getSessionFactory().openSession();
        Query loanQuery = session.createQuery("from LoanEntity");
        List<LoanEntity> loans = loanQuery.list();
        if (loans != null) {
            LoggingUtility.writeLog("تعداد " + loans.size() + " تسهیلات با موفقیت از پایگاه داده استخراج شد.");
            return loans;
        } else {
            LoggingUtility.writeLog(" تسهیلاتی وجود ندارد.");
            return null;
        }
    }

    public LoanEntity readLoanByName(String loanName) {
        try (Session session = HibernateUtility.getSessionFactory().openSession()) {
            Query loanQuery = session.createQuery("from LoanEntity where loanName=:loanName");
            loanQuery.setParameter("loanName", loanName);
            LoanEntity loan = (LoanEntity) loanQuery.list().get(0);
            LoggingUtility.writeLog("اطلاعات تسهیلات با نام " + loan.getLoanName() + " با موفقیت از پایگاه داده استخراج شد.");
            return loan;
        } catch (Exception e) {
            LoggingUtility.writeLog("اطلاعات تسهیلاتی با این نام در سیستم موجود نیست");
            return null;
        }
    }
}
