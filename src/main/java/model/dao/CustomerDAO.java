package model.dao;

import model.entity.CustomerEntity;
import utility.LoggingUtility;
import utility.HibernateUtility;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.TemporalType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CustomerDAO {

    public void createCustomer(CustomerEntity customer) {
        Transaction transaction = null;
        try (Session session = HibernateUtility.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(customer);
            transaction.commit();
            LoggingUtility.writeLog("اطلاعات مشتری شماره " + customer.getCustomerId() + " با موفقیت به پایگاه داده افزوده شد.");
        } catch (Exception e) {
            if (transaction != null) {
                LoggingUtility.writeLog("خطا در ایجاد مشتری جدید - در حال انجام عملیات بازگشت (rollback)");
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public CustomerEntity readCustomer(long id) {
        Transaction transaction;
        Session session = HibernateUtility.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        try {
            CustomerEntity customer = session.byId(CustomerEntity.class).getReference(id);
            transaction.commit();
            LoggingUtility.writeLog("اطلاعات مشتری شماره " + customer.getCustomerId() + " با موفقیت از پایگاه داده استخراج شد.");
            session.close();
            return customer;
        } catch (Exception e) {
            System.out.println("EXCEPTION HANDLED IN CUSTOMER DAO");
            session.close();
            return null;
        }
    }

    public void updateCustomer(CustomerEntity customer) {
        Transaction transaction = null;
        try (Session session = HibernateUtility.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.update(customer);
            transaction.commit();
            LoggingUtility.writeLog("اطلاعات مشتری شماره " + customer.getCustomerId() + " با موفقیت به روزرسانی شد.");
        } catch (Exception e) {
            if (transaction != null) {
                LoggingUtility.writeLog("خطا در به روزرسانی - در حال انجام عملیات بازگشت (rollback)");
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void deleteCustomer(long id) {
        Transaction transaction = null;
        try (Session session = HibernateUtility.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            CustomerEntity customer = session.byId(CustomerEntity.class).getReference(id);
            session.delete(customer);
            transaction.commit();
            LoggingUtility.writeLog("اطلاعات مشتری شماره " + customer.getCustomerId() + " با موفقیت از پایگاه داده حذف شد.");
        } catch (Exception e) {
            if (transaction != null) {
                LoggingUtility.writeLog("خطا در حذف مشتری - در حال انجام عملیات بازگشت (rollback)");
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public List<CustomerEntity> readCustomersByParameters(Boolean customerType, String customerId, String customerCode, String customerName, String customerFamily, String birthDateFrom, String birthDateTo) throws ParseException {
        Session session = HibernateUtility.getSessionFactory().openSession();
        List<CustomerEntity> customers;
        if (birthDateFrom.equals(""))
            birthDateFrom = "0000-00-00";
        if (birthDateTo.equals(""))
            birthDateTo = "9999-00-00";
        if (customerId.equals(""))
            if (customerCode.equals("")) {
                Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(birthDateFrom);
                Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(birthDateTo);
                Query customerQuery = session.createQuery("from CustomerEntity where customerType=:customerType and " +
                        "customerName like :customerName and " +
                        "customerFamily like :customerFamily and " +
                        "customerBirthdate between :birthDateFrom and :birthDateTo");
                customerQuery.setParameter("customerType", customerType);
                customerQuery.setParameter("customerName", customerName + "%");
                customerQuery.setParameter("customerFamily", customerFamily + "%");
                customerQuery.setParameter("birthDateFrom", startDate, TemporalType.DATE);
                customerQuery.setParameter("birthDateTo", endDate, TemporalType.DATE);
                customers = customerQuery.list();
            } else {
                Query customerQuery = session.createQuery("from CustomerEntity where customerType=:customerType and " +
                        "customerCode=:customerCode");
                customerQuery.setParameter("customerType", customerType);
                customerQuery.setParameter("customerCode", customerCode);
                customers = customerQuery.list();
            }
        else {
            Query customerQuery = session.createQuery("from CustomerEntity where customerType=:customerType and " +
                    "customerId=:customerId");
            customerQuery.setParameter("customerType", customerType);
            customerQuery.setParameter("customerId", Long.parseLong(customerId));
            customers = customerQuery.list();
        }
        return customers;
    }
}

