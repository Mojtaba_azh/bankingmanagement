package model.dao;

import model.entity.GrantEntity;
import model.entity.LoanEntity;
import utility.HibernateUtility;
import utility.LoggingUtility;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public class GrantDAO {

    public void createGrant(GrantEntity grant) {
        Transaction transaction = null;
        try (Session session = HibernateUtility.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(grant);
            transaction.commit();
            LoggingUtility.writeLog("اطلاعات شرط اعطا شماره " + grant.getGrantedId() + " با موفقیت به پایگاه داده افزوده شد.");
        } catch (Exception e) {
            if (transaction != null) {
                LoggingUtility.writeLog("خطا در ایجاد شرط اعطا جدید - در حال انجام عملیات بازگشت (rollback)");
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public List<GrantEntity> readGrantsByLoanId(long id) {
        Session session = HibernateUtility.getSessionFactory().openSession();
        LoanEntity loanEntity = session.byId(LoanEntity.class).getReference(id);
        Query grantQuery = session.createQuery("from GrantEntity where loanByLoanId=:loan");
        grantQuery.setParameter("loan", loanEntity);
        List<GrantEntity> grants = grantQuery.list();
        if (grants != null) {
            LoggingUtility.writeLog("تعداد " + grants.size() + " شرط اعطا با موفقیت از پایگاه داده استخراج شد.");
            session.close();
            return grants;
        } else {
            LoggingUtility.writeLog(" شرط اعطایی برای این تسهیلات وجود ندارد.");
            session.close();
            return null;
        }
    }

    public void deleteGrant(long id) {
        Transaction transaction;
        Session session = HibernateUtility.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        try {
            GrantEntity grant = session.byId(GrantEntity.class).getReference(id);
            session.delete(grant);
            transaction.commit();
            LoggingUtility.writeLog("اطلاعات شرط اعطا شماره " + grant.getGrantedId() + " با موفقیت از پایگاه داده حذف شد.");
        } catch (EntityNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            if (transaction != null) {
                LoggingUtility.writeLog("خطا در حذف شرط اعطا - در حال انجام عملیات بازگشت (rollback)");
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
