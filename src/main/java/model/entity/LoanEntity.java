package model.entity;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "loan", schema = "bank")
public class LoanEntity {
    private Long loanId;
    private String loanName;
    private Long loanRate;
    private Collection<CustomerEntity> customersByLoanId;
    private Collection<GrantEntity> grantsByLoanId;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "loan_id")
    public Long getLoanId() {
        return loanId;
    }

    public void setLoanId(Long loanId) {
        this.loanId = loanId;
    }

    @Basic
    @Column(name = "loan_name")
    public String getLoanName() {
        return loanName;
    }

    public void setLoanName(String loanName) {
        this.loanName = loanName;
    }

    @Basic
    @Column(name = "loan_rate")
    public Long getLoanRate() {
        return loanRate;
    }

    public void setLoanRate(Long loanRate) {
        this.loanRate = loanRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoanEntity that = (LoanEntity) o;

        if (loanId != null ? !loanId.equals(that.loanId) : that.loanId != null) return false;
        if (loanName != null ? !loanName.equals(that.loanName) : that.loanName != null) return false;
        if (loanRate != null ? !loanRate.equals(that.loanRate) : that.loanRate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = loanId != null ? loanId.hashCode() : 0;
        result = 31 * result + (loanName != null ? loanName.hashCode() : 0);
        result = 31 * result + (loanRate != null ? loanRate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LoanEntity{" +
                "loanId=" + loanId +
                ", loanName='" + loanName + '\'' +
                ", loanRate=" + loanRate +
                '}';
    }

    @OneToMany(mappedBy = "loanByLoanId")
    public Collection<CustomerEntity> getCustomersByLoanId() {
        return customersByLoanId;
    }

    public void setCustomersByLoanId(Collection<CustomerEntity> customersByLoanId) {
        this.customersByLoanId = customersByLoanId;
    }

    @OneToMany(mappedBy = "loanByLoanId")
    public Collection<GrantEntity> getGrantsByLoanId() {
        return grantsByLoanId;
    }

    public void setGrantsByLoanId(Collection<GrantEntity> grantsByLoanId) {
        this.grantsByLoanId = grantsByLoanId;
    }
}
