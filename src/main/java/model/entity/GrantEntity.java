package model.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "grant_condition", schema = "bank")
public class GrantEntity {
    private Long grantedId;
    private String grantedName;
    private Long minimumDuration;
    private Long maximumDuration;
    private BigDecimal minimumAmount;
    private BigDecimal maximumAmount;
    private LoanEntity loanByLoanId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "granted_id")
    public Long getGrantedId() {
        return grantedId;
    }

    public void setGrantedId(Long grantedId) {
        this.grantedId = grantedId;
    }

    @Basic
    @Column(name = "granted_name")
    public String getGrantedName() {
        return grantedName;
    }

    public void setGrantedName(String grantedName) {
        this.grantedName = grantedName;
    }

    @Basic
    @Column(name = "minimum_duration")
    public Long getMinimumDuration() {
        return minimumDuration;
    }

    public void setMinimumDuration(Long minimumDuration) {
        this.minimumDuration = minimumDuration;
    }

    @Basic
    @Column(name = "maximum_duration")
    public Long getMaximumDuration() {
        return maximumDuration;
    }

    public void setMaximumDuration(Long maximumDuration) {
        this.maximumDuration = maximumDuration;
    }

    @Basic
    @Column(name = "minimum_amount")
    public BigDecimal getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(BigDecimal minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    @Basic
    @Column(name = "maximum_amount")
    public BigDecimal getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(BigDecimal maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GrantEntity that = (GrantEntity) o;

        if (grantedId != null ? !grantedId.equals(that.grantedId) : that.grantedId != null) return false;
        if (grantedName != null ? !grantedName.equals(that.grantedName) : that.grantedName != null) return false;
        if (minimumDuration != null ? !minimumDuration.equals(that.minimumDuration) : that.minimumDuration != null)
            return false;
        if (maximumDuration != null ? !maximumDuration.equals(that.maximumDuration) : that.maximumDuration != null)
            return false;
        if (minimumAmount != null ? !minimumAmount.equals(that.minimumAmount) : that.minimumAmount != null)
            return false;
        if (maximumAmount != null ? !maximumAmount.equals(that.maximumAmount) : that.maximumAmount != null)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = grantedId != null ? grantedId.hashCode() : 0;
        result = 31 * result + (grantedName != null ? grantedName.hashCode() : 0);
        result = 31 * result + (minimumDuration != null ? minimumDuration.hashCode() : 0);
        result = 31 * result + (maximumDuration != null ? maximumDuration.hashCode() : 0);
        result = 31 * result + (minimumAmount != null ? minimumAmount.hashCode() : 0);
        result = 31 * result + (maximumAmount != null ? maximumAmount.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "GrantEntity{" +
                "grantId=" + grantedId +
                ", grantName='" + grantedName + '\'' +
                ", minimumDuration=" + minimumDuration +
                ", maximumDuration=" + maximumDuration +
                ", minimumAmount=" + minimumAmount +
                ", maximumAmount=" + maximumAmount +
                ", loanByLoanId=" + loanByLoanId +
                '}';
    }

    @ManyToOne
    @JoinColumn(name = "loan_id", referencedColumnName = "loan_id")
    public LoanEntity getLoanByLoanId() {
        return loanByLoanId;
    }

    public void setLoanByLoanId(LoanEntity loanByLoanId) {
        this.loanByLoanId = loanByLoanId;
    }
}
