package model.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "customer", schema = "bank")
public class CustomerEntity {
    private Long customerId;
    private String customerCode;
    private boolean customerType;
    private String customerName;
    private String customerFamily;
    private String customerFather;
    private Date customerBirthdate;
    private Long loanDuration;
    private BigDecimal loanAmount;
    private LoanEntity loanByLoanId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id")
    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Basic
    @Column(name = "customer_code")
    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    @Basic
    @Column(name = "customer_type")
    public boolean getCustomerType() {
        return customerType;
    }

    public void setCustomerType(boolean customerType) {
        this.customerType = customerType;
    }

    @Basic
    @Column(name = "customer_name")
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Basic
    @Column(name = "customer_family")
    public String getCustomerFamily() {
        return customerFamily;
    }

    public void setCustomerFamily(String customerFamily) {
        this.customerFamily = customerFamily;
    }

    @Basic
    @Column(name = "customer_father")
    public String getCustomerFather() {
        return customerFather;
    }

    public void setCustomerFather(String customerFather) {
        this.customerFather = customerFather;
    }

    @Basic
    @Column(name = "customer_birthdate")
    @Temporal(TemporalType.DATE)
    public Date getCustomerBirthdate() {
        return customerBirthdate;
    }

    public void setCustomerBirthdate(Date customerBirthdate) {
        this.customerBirthdate = customerBirthdate;
    }

    @Basic
    @Column(name = "loan_duration")
    public Long getLoanDuration() {
        return loanDuration;
    }

    public void setLoanDuration(Long loanDuration) {
        this.loanDuration = loanDuration;
    }

    @Basic
    @Column(name = "loan_amount")
    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerEntity that = (CustomerEntity) o;

        if (customerType != that.customerType) return false;
        if (customerId != null ? !customerId.equals(that.customerId) : that.customerId != null) return false;
        if (customerCode != null ? !customerCode.equals(that.customerCode) : that.customerCode != null) return false;
        if (customerName != null ? !customerName.equals(that.customerName) : that.customerName != null) return false;
        if (customerFamily != null ? !customerFamily.equals(that.customerFamily) : that.customerFamily != null)
            return false;
        if (customerFather != null ? !customerFather.equals(that.customerFather) : that.customerFather != null)
            return false;
        if (customerBirthdate != null ? !customerBirthdate.equals(that.customerBirthdate) : that.customerBirthdate != null)
            return false;
        if (loanDuration != null ? !loanDuration.equals(that.loanDuration) : that.loanDuration != null) return false;
        if (loanAmount != null ? !loanAmount.equals(that.loanAmount) : that.loanAmount != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = customerId != null ? customerId.hashCode() : 0;
        result = 31 * result + (customerCode != null ? customerCode.hashCode() : 0);
        result = 31 * result + (customerType ? 1 : 0);
        result = 31 * result + (customerName != null ? customerName.hashCode() : 0);
        result = 31 * result + (customerFamily != null ? customerFamily.hashCode() : 0);
        result = 31 * result + (customerFather != null ? customerFather.hashCode() : 0);
        result = 31 * result + (customerBirthdate != null ? customerBirthdate.hashCode() : 0);
        result = 31 * result + (loanDuration != null ? loanDuration.hashCode() : 0);
        result = 31 * result + (loanAmount != null ? loanAmount.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CustomerEntity{" +
                "customerId=" + customerId +
                ", customerCode='" + customerCode + '\'' +
                ", customerType=" + customerType +
                ", customerName='" + customerName + '\'' +
                ", customerFamily='" + customerFamily + '\'' +
                ", customerFather='" + customerFather + '\'' +
                ", customerBirthdate=" + customerBirthdate +
                ", loanDuration=" + loanDuration +
                ", loanAmount=" + loanAmount +
                ", loanByLoanId=" + loanByLoanId +
                '}';
    }

    @ManyToOne
    @JoinColumn(name = "loan_id", referencedColumnName = "loan_id")
    public LoanEntity getLoanByLoanId() {
        return loanByLoanId;
    }

    public void setLoanByLoanId(LoanEntity loanByLoanId) {
        this.loanByLoanId = loanByLoanId;
    }
}
