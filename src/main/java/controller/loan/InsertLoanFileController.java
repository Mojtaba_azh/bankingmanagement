package controller.loan;

import model.dao.CustomerDAO;
import model.dao.GrantDAO;
import model.dao.LoanDAO;
import model.entity.CustomerEntity;
import model.entity.GrantEntity;
import model.entity.LoanEntity;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@WebServlet("/insert-loan-file")
public class InsertLoanFileController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        long loanId = Long.parseLong(request.getParameter("loanName"));
        BigDecimal loanAmount = new BigDecimal(request.getParameter("loanAmount"));
        Long loanDuration = Long.parseLong(request.getParameter("loanDuration"));
        GrantDAO grantDAO = new GrantDAO();
        List<GrantEntity> grants = grantDAO.readGrantsByLoanId(loanId);
        boolean canInsertLoanFile = false;
        for (GrantEntity grant : grants
        ) {
            boolean amountValid = validateAmountInRange(grant.getMinimumAmount(), loanAmount, grant.getMaximumAmount());
            boolean durationValid = validateDurationInRange(grant.getMinimumDuration(), loanDuration, grant.getMaximumDuration());
            if (durationValid && amountValid)
                canInsertLoanFile = true;
        }
        if (canInsertLoanFile) {
            CustomerDAO customerDAO = new CustomerDAO();
            long customerId = Long.parseLong(request.getParameter("customerId"));
            CustomerEntity customer = customerDAO.readCustomer(customerId);
            LoanDAO loanDAO = new LoanDAO();
            LoanEntity loan = loanDAO.readLoan(loanId);
            customer.setLoanByLoanId(loan);
            customer.setLoanAmount(loanAmount);
            customer.setLoanDuration(loanDuration);
            customerDAO.updateCustomer(customer);
            request.setAttribute("loanedCustomerId", customer.getCustomerId());
            RequestDispatcher dispatcher = request.getRequestDispatcher("success-loan-file.jsp");
            dispatcher.forward(request, response);
        } else {
            ResourceBundle bundle;
            if (request.getSession().getAttribute("selectedLanguage").equals("Persian")) {
                Locale persianLocale = new Locale("fa", "IR");
                bundle = ResourceBundle.getBundle("/MessageBundles/fa/MessageBundle", persianLocale);
            } else {
                Locale englishLocale = new Locale("en", "US");
                bundle = ResourceBundle.getBundle("/MessageBundles/en/MessageBundle", englishLocale);
            }
            request.setAttribute("errorMessage", bundle.getString("grantConditionError"));
            RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
            dispatcher.forward(request, response);
        }
    }

    private static boolean validateDurationInRange(Long minimumDuration, Long loanDuration, Long maximumDuration) {
        if (loanDuration < 0)
            return false;
        if (loanDuration < minimumDuration)
            return false;
        return loanDuration <= maximumDuration;
    }

    private static boolean validateAmountInRange(BigDecimal minimumAmount, BigDecimal loanAmount, BigDecimal maximumAmount) {
        if (loanAmount.compareTo(BigDecimal.ZERO) < 0)
            return false;
        if (loanAmount.compareTo(minimumAmount) < 0)
            return false;
        return loanAmount.compareTo(maximumAmount) <= 0;
    }
}
