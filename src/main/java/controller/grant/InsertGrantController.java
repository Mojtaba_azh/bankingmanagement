package controller.grant;

import model.dao.GrantDAO;
import model.dao.LoanDAO;
import model.entity.GrantEntity;
import model.entity.LoanEntity;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@WebServlet("/insert-grant")
public class InsertGrantController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        if (request.getParameter("grantedName")==null) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("add-grant.jsp");
            dispatcher.forward(request, response);
        } else {
            String grantedName = request.getParameter("grantedName");
            BigDecimal minimumAmount = new BigDecimal(request.getParameter("minimumAmount"));
            BigDecimal maximumAmount = new BigDecimal(request.getParameter("maximumAmount"));
            Long minimumDuration = Long.parseLong(request.getParameter("minimumDuration"));
            Long maximumDuration = Long.parseLong(request.getParameter("maximumDuration"));
            LoanEntity loan = (LoanEntity) request.getSession().getAttribute("loan");
            LoanDAO loanDAO = new LoanDAO();
            if (loan.getLoanId() == null)
                loanDAO.createLoan(loan);
            GrantEntity newGrant = new GrantEntity();
            newGrant.setGrantedName(grantedName);
            newGrant.setMinimumAmount(minimumAmount);
            newGrant.setMaximumAmount(maximumAmount);
            newGrant.setMinimumDuration(minimumDuration);
            newGrant.setMaximumDuration(maximumDuration);
            newGrant.setLoanByLoanId(loan);
            GrantDAO grantDAO = new GrantDAO();
            grantDAO.createGrant(newGrant);
            List<GrantEntity> grants = grantDAO.readGrantsByLoanId(loan.getLoanId());
            request.getSession().setAttribute("grants", grants);
            RequestDispatcher dispatcher = request.getRequestDispatcher("add-grant.jsp");
            dispatcher.forward(request, response);
        }
    }
}
