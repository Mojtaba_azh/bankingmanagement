package controller.grant;

import model.dao.GrantDAO;
import model.entity.GrantEntity;
import model.entity.LoanEntity;

import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/delete-grant")
public class DeleteGrantController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            String grantedId = request.getParameter("id");
            GrantDAO grantDAO = new GrantDAO();
            LoanEntity loan = (LoanEntity) request.getSession().getAttribute("loan");
            List<GrantEntity> grants;
            try {
                grantDAO.deleteGrant(Long.parseLong(grantedId));
                grants = grantDAO.readGrantsByLoanId(loan.getLoanId());
                request.getSession().setAttribute("grants", grants);
            } catch (Exception e) {
                System.out.println("There is no selected grant to delete");
            }
            RequestDispatcher dispatcher = request.getRequestDispatcher("add-grant.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
