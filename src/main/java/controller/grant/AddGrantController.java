package controller.grant;

import model.dao.GrantDAO;
import model.dao.LoanDAO;
import model.entity.GrantEntity;
import model.entity.LoanEntity;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/add-grant")
public class AddGrantController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String loanName;
        Long loanRate;
        if (request.getParameter("loanName") != null && request.getParameter("loanRate") != null) {
            request.getSession().setAttribute("loanName", request.getParameter("loanName"));
            request.getSession().setAttribute("loanRate", Long.parseLong(request.getParameter("loanRate")));
        } else if (request.getSession().getAttribute("loanName") == null && request.getSession().getAttribute("loanRate") == null) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("add-loan.jsp");
            dispatcher.forward(request, response);
        }
        loanName = (String) request.getSession().getAttribute("loanName");
        loanRate = (Long) request.getSession().getAttribute("loanRate");
        LoanDAO loanDAO = new LoanDAO();
        LoanEntity loan;
        loan = loanDAO.readLoanByName(loanName);
        if (loan == null) {
            loan = new LoanEntity();
            loan.setLoanName(loanName);
            loan.setLoanRate(loanRate);
        }
        request.getSession().setAttribute("loan", loan);
        GrantDAO grantDAO = new GrantDAO();
        List<GrantEntity> grants;
        try {
            grants = grantDAO.readGrantsByLoanId(loan.getLoanId());
        } catch (NullPointerException e) {
            grants = null;
        }
        request.getSession().setAttribute("grants", grants);
        RequestDispatcher dispatcher = request.getRequestDispatcher("add-grant.jsp");
        dispatcher.forward(request, response);
    }
}
