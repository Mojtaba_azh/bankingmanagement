package controller.grant;

import model.dao.GrantDAO;
import model.entity.GrantEntity;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/show-grants")
public class ShowGrantsController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        long loanId = (Long) request.getAttribute("loanId");
        GrantDAO grantDAO = new GrantDAO();
        List<GrantEntity> grants = grantDAO.readGrantsByLoanId(loanId);
        request.setAttribute("grants", grants);
        RequestDispatcher dispatcher = request.getRequestDispatcher("grants.jsp");
        dispatcher.forward(request, response);
    }
}
