package controller.customer;

import model.dao.CustomerDAO;
import model.entity.CustomerEntity;
import utility.DateConverter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.Locale;
import java.util.ResourceBundle;

@WebServlet("/insert-customer")
public class InsertCustomerController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        boolean customerType = true;
        if (request.getParameter("customerType").equals("legal"))
            customerType = false;
        CustomerEntity newCustomer = new CustomerEntity();
        newCustomer.setCustomerType(customerType);
        newCustomer.setCustomerCode(request.getParameter("customerCode"));
        newCustomer.setCustomerName(request.getParameter("customerName"));
        newCustomer.setCustomerFamily(request.getParameter("customerFamily"));
        newCustomer.setCustomerFather(request.getParameter("customerFather"));
        if (request.getSession().getAttribute("selectedLanguage").equals("Persian"))
            newCustomer.setCustomerBirthdate(Date.valueOf(DateConverter.convertToGregorian(request.getParameter("customerBirthDate"))));
        else
            newCustomer.setCustomerBirthdate(Date.valueOf((request.getParameter("customerBirthDate"))));
        try {
            CustomerDAO customerDAO = new CustomerDAO();
            customerDAO.createCustomer(newCustomer);
            request.setAttribute("newCustomerId", newCustomer.getCustomerId());
            RequestDispatcher dispatcher = request.getRequestDispatcher("success-insert-customer.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e) {
            ResourceBundle bundle;
            if (request.getSession().getAttribute("selectedLanguage").equals("Persian")) {
                Locale persianLocale = new Locale("fa", "IR");
                bundle = ResourceBundle.getBundle("/MessageBundles/fa/MessageBundle", persianLocale);
            } else {
                Locale englishLocale = new Locale("en", "US");
                bundle = ResourceBundle.getBundle("/MessageBundles/en/MessageBundle", englishLocale);
            }
            request.setAttribute("errorMessage", bundle.getString("duplicateCustomerError"));
            RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
            dispatcher.forward(request, response);
        }
    }
}
