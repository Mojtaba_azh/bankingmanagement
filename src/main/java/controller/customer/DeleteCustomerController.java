package controller.customer;

import model.dao.CustomerDAO;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/delete-customer")
public class DeleteCustomerController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            String customerId = request.getParameter("id");
            CustomerDAO customerDAO = new CustomerDAO();
            customerDAO.deleteCustomer(Long.parseLong(customerId));
            response.sendRedirect("show-customer");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
