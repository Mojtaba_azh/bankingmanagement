package controller.customer;

import model.dao.CustomerDAO;
import model.entity.CustomerEntity;

import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/edit-customer")
public class EditCustomerController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            String customerId = request.getParameter("id");
            boolean customerType = true;
            if (request.getParameter("type").equals("legal"))
                customerType = false;
            CustomerEntity existingCustomer;
            CustomerDAO customerDAO = new CustomerDAO();
            existingCustomer = customerDAO.readCustomer(Long.parseLong(customerId));
            request.setAttribute("customer", existingCustomer);
            request.setAttribute("customerType", customerType);
            request.setAttribute("jalaliBirthDate", request.getParameter("jalaliBirthDate"));
            RequestDispatcher dispatcher = request.getRequestDispatcher("edit-customer.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
