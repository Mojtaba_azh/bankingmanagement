package controller.customer;

import model.dao.CustomerDAO;
import model.dao.LoanDAO;
import model.entity.CustomerEntity;
import model.entity.LoanEntity;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/show-customer-by-id")
public class ShowCustomerByIdController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        if (request.getParameter("customerIdToFind") != null) {
            request.getSession().setAttribute("customerIdToFind", Long.parseLong(request.getParameter("customerIdToFind")));
        } else if (request.getSession().getAttribute("customerIdToFind") == null) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("add-loan-file.jsp");
            dispatcher.forward(request, response);
        }
        Long customerIdToFind = (Long) request.getSession().getAttribute("customerIdToFind");
        CustomerEntity customer;
        try {
            CustomerDAO customerDAO = new CustomerDAO();
            customer = customerDAO.readCustomer(customerIdToFind);
            request.setAttribute("customer", customer);
            LoanDAO loanDAO = new LoanDAO();
            List<LoanEntity> loans = loanDAO.readLoans();
            request.setAttribute("loans", loans);
            RequestDispatcher dispatcher = request.getRequestDispatcher("add-loan-file.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
