package controller.customer;

import utility.DateConverter;

import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/select-customer")
public class SelectCustomerController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("UTF-8");
        boolean customerType = true;
        if (request.getParameter("customerType").equals("legal"))
            customerType = false;
        request.getSession().setAttribute("customerTypeToFind", customerType);
        request.getSession().setAttribute("customerIdToFind", request.getParameter("customerId"));
        request.getSession().setAttribute("customerCodeToFind", request.getParameter("customerCode"));
        request.getSession().setAttribute("customerNameToFind", request.getParameter("customerName"));
        request.getSession().setAttribute("customerFamilyToFind", request.getParameter("customerFamily"));
        if (request.getParameter("birthDateFrom").equals("") || request.getParameter("birthDateFrom") == null)
            request.getSession().setAttribute("birthDateFrom", "");
        else {
            if (request.getSession().getAttribute("selectedLanguage").equals("Persian"))
                request.getSession().setAttribute("birthDateFrom", DateConverter.convertToGregorian(request.getParameter("birthDateFrom")));
            else
                request.getSession().setAttribute("birthDateFrom", request.getParameter("birthDateFrom"));
        }
        if (request.getParameter("birthDateTo").equals("") || request.getParameter("birthDateTo") == null)
            request.getSession().setAttribute("birthDateTo", "");
        else {
            if (request.getSession().getAttribute("selectedLanguage").equals("Persian"))
                request.getSession().setAttribute("birthDateTo", DateConverter.convertToGregorian(request.getParameter("birthDateTo")));
            else
                request.getSession().setAttribute("birthDateTo", request.getParameter("birthDateTo"));
        }
        response.sendRedirect("show-customer");
    }
}
