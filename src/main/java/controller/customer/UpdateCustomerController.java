package controller.customer;

import model.dao.CustomerDAO;
import model.entity.CustomerEntity;
import org.hibernate.exception.ConstraintViolationException;
import utility.DateConverter;

import java.io.IOException;
import java.sql.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/update-customer")
public class UpdateCustomerController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String customerId = request.getParameter("id");
        boolean customerType = Boolean.parseBoolean(request.getParameter("type"));
        String customerCode = request.getParameter("customerCode");
        String customerName = request.getParameter("customerName");
        String customerFamily = "";
        Date customerBirthDate;
        if (request.getParameter("customerFamily") != null)
            customerFamily = request.getParameter("customerFamily");
        String customerFather = "";
        if (request.getParameter("customerFather") != null)
            customerFather = request.getParameter("customerFather");
        if (request.getSession().getAttribute("selectedLanguage").equals("Persian"))
            customerBirthDate = Date.valueOf(DateConverter.convertToGregorian(request.getParameter("customerBirthDate")));
        else
            customerBirthDate = Date.valueOf(request.getParameter("customerBirthDate"));
        CustomerEntity customer = new CustomerEntity();
        customer.setCustomerId(Long.parseLong(customerId));
        customer.setCustomerCode(customerCode);
        customer.setCustomerName(customerName);
        customer.setCustomerFamily(customerFamily);
        customer.setCustomerFather(customerFather);
        customer.setCustomerBirthdate(customerBirthDate);
        customer.setCustomerType(customerType);
        try {
            CustomerDAO customerDAO = new CustomerDAO();
                customerDAO.updateCustomer(customer);
                response.sendRedirect("show-customer");
        } catch (Exception e) {
            ResourceBundle bundle;
            if (request.getSession().getAttribute("selectedLanguage").equals("Persian")) {
                Locale persianLocale = new Locale("fa", "IR");
                bundle = ResourceBundle.getBundle("/MessageBundles/fa/MessageBundle", persianLocale);
            } else {
                Locale englishLocale = new Locale("en", "US");
                bundle = ResourceBundle.getBundle("/MessageBundles/en/MessageBundle", englishLocale);
            }
            request.setAttribute("errorMessage", bundle.getString("duplicateCustomerError"));
            RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
            dispatcher.forward(request, response);
        }
    }
}
