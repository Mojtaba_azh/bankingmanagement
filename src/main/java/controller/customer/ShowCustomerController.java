package controller.customer;

import model.dao.CustomerDAO;
import model.entity.CustomerEntity;

import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/show-customer")
public class ShowCustomerController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        boolean customerTypeToFind = (boolean) request.getSession().getAttribute("customerTypeToFind");
        String customerIdToFind = (String) request.getSession().getAttribute("customerIdToFind");
        String customerCodeToFind = (String) request.getSession().getAttribute("customerCodeToFind");
        String customerNameToFind = (String) request.getSession().getAttribute("customerNameToFind");
        String customerFamilyToFind = (String) request.getSession().getAttribute("customerFamilyToFind");
        String birthDateFrom = (String) request.getSession().getAttribute("birthDateFrom");
        String birthDateTo = (String) request.getSession().getAttribute("birthDateTo");
        CustomerDAO customerDAO = new CustomerDAO();
        List<CustomerEntity> customers;
        try {
            customers = customerDAO.readCustomersByParameters(customerTypeToFind, customerIdToFind, customerCodeToFind, customerNameToFind, customerFamilyToFind, birthDateFrom, birthDateTo);
            request.setAttribute("customers", customers);
            request.setAttribute("customerType", customerTypeToFind);
            RequestDispatcher dispatcher = request.getRequestDispatcher("customers.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
