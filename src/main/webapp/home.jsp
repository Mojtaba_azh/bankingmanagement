<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    request.setCharacterEncoding("UTF-8");
    Locale persianLocale = new Locale("fa", "IR");
    Locale englishLocale = new Locale("en", "US");
    ResourceBundle bundle = null;
    String selectedLanguage = (String) request.getSession().getAttribute("selectedLanguage");
    if (selectedLanguage == null || selectedLanguage.equals("Persian")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/fa/MessageBundle", persianLocale);
    } else if (selectedLanguage.equals("English")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/en/MessageBundle", englishLocale);
    }
    assert bundle != null;%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><%=bundle.getString("homeTitle")%>
    </title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <script src="js/functions.js"></script>
</head>
<%
    assert selectedLanguage != null;
    if (selectedLanguage.equals("Persian")) {
%>
<body style="direction: rtl">
    <%
} else {
%>
<body style="direction: ltr">
<%
    }
%>
<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: darkgray; height: 60px;">
        <form name="languageForm" method="post">
            <input type="hidden" name="selectedLanguage" value="English">
            <div class="row">
                <div class="col">
                    <button class="btn btn-light btn-sm btn-block" type="button" onclick="changeLanguageToPersian()">
                        فارسی
                    </button>
                </div>
                <div class="col">
                    <button class="btn btn-dark btn-sm btn-block" type="button" onclick="changeLanguageToEnglish()">
                        English
                    </button>
                </div>
            </div>
        </form>
    </nav>
</header>

<br>
<%
    if (selectedLanguage.equals("Persian")) {
%>
<div class="container text-right">
        <%
        } else {
    %>
    <div class="container text-left">
        <%
            }
        %>
        <div style="padding: 10px; margin: 20px;">
            <h1><%=bundle.getString("homeWelcome")%>
            </h1>
            <h2><%=bundle.getString("homeChoose")%>
            </h2>
        </div>
        <br>
        <div class="row">
            <div class="col">
                <a href="<%=request.getContextPath()%>/add-customer"
                   class="btn btn-outline-primary btn-lg btn-block">
                    <%=bundle.getString("addCustomer")%>
                </a>
            </div>
            <div class="col">
                <a href="<%=request.getContextPath()%>/find-customer"
                   class="btn btn-outline-danger btn-lg btn-block">
                    <%=bundle.getString("findCustomers")%>
                </a>
            </div>
            <div class="col">
                <a href="<%=request.getContextPath()%>/add-loan"
                   class="btn btn-outline-warning btn-lg btn-block">
                    <%=bundle.getString("addLoan")%>
                </a>
            </div>
            <div class="col">
                <a href="<%=request.getContextPath()%>/add-loan-file"
                   class="btn btn-outline-success btn-lg btn-block">
                    <%=bundle.getString("addLoanFile")%>
                </a>
            </div>
        </div>
    </div>
</body>
</html>
