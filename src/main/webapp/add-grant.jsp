<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>
<%@ page import="model.entity.LoanEntity" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    request.setCharacterEncoding("UTF-8");
    Locale persianLocale = new Locale("fa", "IR");
    Locale englishLocale = new Locale("en", "US");
    ResourceBundle bundle = null;
    String selectedLanguage = (String) request.getSession().getAttribute("selectedLanguage");
    if (selectedLanguage.equals("Persian")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/fa/MessageBundle", persianLocale);
    } else if (selectedLanguage.equals("English")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/en/MessageBundle", englishLocale);
    }
    String loanName = ((LoanEntity) request.getSession().getAttribute("loan")).getLoanName();
    String textBoxLanguageClass;
    assert bundle != null;%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><%=bundle.getString("addGrant")%>
    </title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link rel="stylesheet" href="css/vertical-responsive-menu.min.css"/>
    <link rel="stylesheet" href="css/persianDatepicker-default.css"/>
    <script src="js/jquery-1.10.1.min.js"></script>
    <script src="js/persianDatepicker.js"></script>
    <script src="js/functions.js"></script>
</head>
<%
    if (selectedLanguage.equals("Persian")) {
        textBoxLanguageClass="persianTextBox";
%>
<body style="direction: rtl">
    <%
} else {
        textBoxLanguageClass="englishTextBox";
%>
<body style="direction: ltr">
<%
    }
%>
<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: #fbbc05;height: 60px;">
        <ul class="navbar-nav">
            <li><a href="<%=request.getContextPath()%>/home" class="nav-link"><%=bundle.getString("homeTitle")%>
            </a></li>
            <li><a href="<%=request.getContextPath()%>/add-loan" class="nav-link"><%=bundle.getString("addLoan")%>
            </a></li>
        </ul>
    </nav>
</header>
<br>
<%
    if (selectedLanguage.equals("Persian")) {
%>
<div class="container text-right">
        <%
    } else {
    %>
    <div class="container text-left">
        <%
            }
        %>
        <div class="card">
            <div class="card-body">
                <form action="insert-grant" method="post" autocomplete="off"
                        <%
                            if (selectedLanguage.equals("Persian")) {
                        %>
                      onsubmit="return validateGrantRangeFA()"
                        <%
                        } else {
                        %>
                      onsubmit="return validateGrantRangeEN()"
                        <%
                            }
                        %>
                >
                    <h2 class="text-center"><%=bundle.getString("addGrantForLoan") + " " + loanName%>
                    </h2>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <fieldset class="form-group">
                                <label id="grantedNameLabel"><%=bundle.getString("grantedName")%>
                                </label>
                                <input id="grantedNameInput" type="text" class="form-control <%=textBoxLanguageClass%>" name="grantedName"
                                       minlength="1"
                                       title="<%=bundle.getString("grantedName")%>" required="required"
                                       oninput="this.setCustomValidity('')"
                                    <%
                               if (selectedLanguage.equals("Persian")) {
                           %>
                                       oninvalid="this.setCustomValidity(invalidInputFA(document.getElementById('grantedNameLabel').innerHTML))"
                                    <%
                                    } else {
                                       %>
                                       oninvalid="this.setCustomValidity(invalidInputEN(document.getElementById('grantedNameLabel').innerHTML))"
                                    <%
                                    }
                                       %>
                                >
                            </fieldset>
                        </div>
                        <div class="col-md-2">
                            <fieldset class="form-group">
                                <label id="minimumAmountLabel"><%=bundle.getString("minimumAmount")%>
                                </label>
                                <input id="minimumAmountInput" type="text" class="form-control numberTextBox" name="minimumAmount"
                                       minlength="1" maxlength="17"
                                       title="<%=bundle.getString("minimumAmount")%>" required="required"
                                       oninput="this.setCustomValidity('')"
                                    <%
                               if (selectedLanguage.equals("Persian")) {
                           %>
                                       oninvalid="this.setCustomValidity(invalidInputFA(document.getElementById('minimumAmountLabel').innerHTML))"
                                    <%
                                    } else {
                                       %>
                                       oninvalid="this.setCustomValidity(invalidInputEN(document.getElementById('minimumAmountLabel').innerHTML))"
                                    <%
                                    }
                                       %>
                                >
                            </fieldset>
                        </div>
                        <div class="col-md-2">
                            <fieldset class="form-group">
                                <label id="maximumAmountLabel"><%=bundle.getString("maximumAmount")%>
                                </label>
                                <input id="maximumAmountInput" type="text" class="form-control numberTextBox" name="maximumAmount"
                                       minlength="1" maxlength="17"
                                       title="<%=bundle.getString("maximumAmount")%>" required="required"
                                       oninput="this.setCustomValidity('')"
                                    <%
                               if (selectedLanguage.equals("Persian")) {
                           %>
                                       oninvalid="this.setCustomValidity(invalidInputFA(document.getElementById('maximumAmountLabel').innerHTML))"
                                    <%
                                    } else {
                                       %>
                                       oninvalid="this.setCustomValidity(invalidInputEN(document.getElementById('maximumAmountLabel').innerHTML))"
                                    <%
                                    }
                                       %>
                                >
                            </fieldset>
                        </div>
                        <div class="col-md-2">
                            <fieldset class="form-group">
                                <label id="minimumDurationLabel"><%=bundle.getString("minimumDuration")%>
                                </label>
                                <input id="minimumDurationInput" type="text" class="form-control numberTextBox" name="minimumDuration"
                                       minlength="1" maxlength="7"
                                       title="<%=bundle.getString("minimumDuration")%>" required="required"
                                       oninput="this.setCustomValidity('')"
                                    <%
                               if (selectedLanguage.equals("Persian")) {
                           %>
                                       oninvalid="this.setCustomValidity(invalidInputFA(document.getElementById('minimumDurationLabel').innerHTML))"
                                    <%
                                    } else {
                                       %>
                                       oninvalid="this.setCustomValidity(invalidInputEN(document.getElementById('minimumDurationLabel').innerHTML))"
                                    <%
                                    }
                                       %>
                                >
                            </fieldset>
                        </div>
                        <div class="col-md-2">
                            <fieldset class="form-group">
                                <label id="maximumDurationLabel"><%=bundle.getString("maximumDuration")%>
                                </label>
                                <input id="maximumDurationInput" type="text" class="form-control numberTextBox" name="maximumDuration"
                                       minlength="1" maxlength="7"
                                       title="<%=bundle.getString("maximumDuration")%>" required="required"
                                       oninput="this.setCustomValidity('')"
                                    <%
                               if (selectedLanguage.equals("Persian")) {
                           %>
                                       oninvalid="this.setCustomValidity(invalidInputFA(document.getElementById('maximumDurationLabel').innerHTML))"
                                    <%
                                    } else {
                                       %>
                                       oninvalid="this.setCustomValidity(invalidInputEN(document.getElementById('maximumDurationLabel').innerHTML))"
                                    <%
                                    }
                                       %>
                                >
                            </fieldset>
                        </div>
                        <div class="col-md-1">
                            <fieldset class="form-group">
                                <label>-</label>
                                <button type="submit" class="btn btn-primary"><%=bundle.getString("register")%>
                                </button>
                            </fieldset>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <jsp:include page="/grants.jsp"/>
            </div>
        </div>
    </div>
</body>
</html>
