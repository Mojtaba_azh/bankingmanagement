<%@ page import="java.util.Locale" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    request.setCharacterEncoding("UTF-8");
    Locale persianLocale = new Locale("fa", "IR");
    Locale englishLocale = new Locale("en", "US");
    ResourceBundle bundle = null;
    String selectedLanguage = (String) request.getSession().getAttribute("selectedLanguage");
    if (selectedLanguage.equals("Persian")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/fa/MessageBundle", persianLocale);
    } else if (selectedLanguage.equals("English")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/en/MessageBundle", englishLocale);
    }
    String textBoxLanguageClass;
    assert bundle != null;%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><%=bundle.getString("findCustomers")%>
    </title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link rel="stylesheet" href="css/vertical-responsive-menu.min.css"/>
    <link rel="stylesheet" href="css/persianDatepicker-default.css"/>
    <script src="js/jquery-1.10.1.min.js"></script>
    <script src="js/persianDatepicker.js"></script>
    <script src="js/functions.js"></script>
</head>
<%
    if (selectedLanguage.equals("Persian")) {
        textBoxLanguageClass = "persianTextBox";
%>
<body style="direction: rtl">
    <%
} else {
        textBoxLanguageClass = "englishTextBox";
%>
<body style="direction: ltr">
<%
    }
%>
<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: #ea4335;height: 60px;">
        <ul class="navbar-nav">
            <li><a href="<%=request.getContextPath()%>/home" class="nav-link"><%=bundle.getString("homeTitle")%>
            </a></li>
        </ul>
    </nav>
</header>
<br>
<%
    if (selectedLanguage.equals("Persian")) {
%>
<div class="container col-md-8 text-right">
        <%
        } else {
    %>
    <div class="container col-md-8 text-left">
        <%
            }
        %>
        <div class="card">
            <div class="card-body">
                <form action="select-customer" method="post" autocomplete="off"
                        <%
                            if (selectedLanguage.equals("Persian")) {
                        %>
                      onsubmit="return validateDateRangeFA()"
                        <%
                        } else {
                        %>
                      onsubmit="return validateDateRangeEN()"
                        <%
                            }
                        %>
                >
                    <h2 class="text-center"><%=bundle.getString("findCustomers")%>
                    </h2>
                    <br><br>
                    <fieldset class="form-group">
                        <div class="row">
                            <div class="col">
                                <h4><label><%=bundle.getString("cType")%>
                                </label></h4>
                            </div>
                            <div class="col">
                                <%
                                    if (selectedLanguage.equals("Persian")) {
                                %>
                                <input type="radio" id="real" name="customerType" value="real" checked
                                       onclick="if(this.checked){showRealContentForFindFA()}">
                                <%
                                } else {
                                %>
                                <input type="radio" id="real" name="customerType" value="real" checked
                                       onclick="if(this.checked){showRealContentForFindEN()}">
                                <%
                                    }
                                %>
                                <label for="real"><%=bundle.getString("realCustomer")%>
                                </label>
                            </div>
                            <div class="col">
                                <%
                                    if (selectedLanguage.equals("Persian")) {
                                %>
                                <input type="radio" id="legal" name="customerType" value="legal"
                                       onclick="if(this.checked){showLegalContentForFindFA()}">
                                <%
                                } else {
                                %>
                                <input type="radio" id="legal" name="customerType" value="legal"
                                       onclick="if(this.checked){showLegalContentForFindEN()}">
                                <%
                                    }
                                %>
                                <label for="legal"><%=bundle.getString("legalCustomer")%>
                                </label>
                            </div>
                        </div>
                    </fieldset>
                    <div class="row">
                        <div class="col">
                            <fieldset class="form-group">
                                <label><%=bundle.getString("customerId")%>
                                </label>
                                <input type="text" class="form-control numberTextBox" name="customerId"
                                       title="<%=bundle.getString("customerId")%>"
                                       oninput="this.setCustomValidity('')">
                            </fieldset>
                        </div>
                        <div class="col">
                            <fieldset class="form-group">
                                <label id="customerCodeLabel"><%=bundle.getString("nationalCode")%>
                                </label>
                                <input id="customerCodeInput" type="text" class="form-control numberTextBox" name="customerCode"
                                       minlength="10" maxlength="10"
                                       title="<%=bundle.getString("nationalCode")%>"
                                       <%
                               if (selectedLanguage.equals("Persian")) {
                           %>oninvalid="this.setCustomValidity(invalidInputCustomerCodeFA(document.getElementById('customerCodeLabel').innerHTML))"
                                    <%
                        } else {
                           %>
                                       oninvalid="this.setCustomValidity(invalidInputCustomerCodeEN(document.getElementById('customerCodeLabel').innerHTML))"
                                    <%
                        }
                           %>
                                       oninput="this.setCustomValidity('')">
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <fieldset class="form-group">
                                <label id="customerNameLabel"><%=bundle.getString("firstName")%>
                                </label>
                                <input id="customerNameInput" type="text" class="form-control <%=textBoxLanguageClass%>" name="customerName"
                                       title="<%=bundle.getString("firstName")%>"
                                       oninput="this.setCustomValidity('')">
                            </fieldset>
                        </div>
                        <div class="col">
                            <fieldset id="customerFamilyField" class="form-group">
                                <label><%=bundle.getString("lastName")%>
                                </label>
                                <input id="customerFamilyInput" type="text" class="form-control <%=textBoxLanguageClass%>" name="customerFamily"
                                       title="<%=bundle.getString("lastName")%>"
                                       oninput="this.setCustomValidity('')">
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <fieldset class="form-group">
                                <label id="customerBirthdateLabelFrom"><%=bundle.getString("birthDateF")%>
                                </label>
                                <%
                                    if (selectedLanguage.equals("Persian")) {
                                %>
                                <input id="customerBirthdateInputFrom" type="text" class="birthDatePicker form-control"
                                       name="birthDateFrom"
                                       pattern="[1-4]\d{3}\/((0?[1-6]\/((3[0-1])|([1-2][0-9])|(0?[1-9])))|((1[0-2]|(0?[7-9]))\/(30|([1-2][0-9])|(0?[1-9]))))"
                                       title="<%=bundle.getString("birthDateF")%>"
                                       oninvalid="this.setCustomValidity(invalidInputDateFA(document.getElementById('customerBirthdateLabelFrom').innerHTML))"
                                       oninput="this.setCustomValidity('')" onfocusout="this.setCustomValidity('')">
                                <%
                                } else {
                                %>
                                <input id="customerBirthdateInputFrom" type="date" class="form-control"
                                       name="birthDateFrom"
                                       title="<%=bundle.getString("birthDateF")%>"
                                       oninvalid="this.setCustomValidity(invalidInputDateEN(document.getElementById('customerBirthdateLabelFrom').innerHTML))"
                                       oninput="this.setCustomValidity('')" onfocusout="this.setCustomValidity('')">
                                <%
                                    }
                                %>
                            </fieldset>
                        </div>
                        <div class="col">
                            <fieldset class="form-group">
                                <label id="customerBirthdateLabelTo"><%=bundle.getString("birthDateT")%>
                                </label>
                                <%
                                    if (selectedLanguage.equals("Persian")) {
                                %>
                                <input id="customerBirthdateInputTo" type="text" class="birthDatePicker form-control"
                                       name="birthDateTo"
                                       pattern="[1-4]\d{3}\/((0?[1-6]\/((3[0-1])|([1-2][0-9])|(0?[1-9])))|((1[0-2]|(0?[7-9]))\/(30|([1-2][0-9])|(0?[1-9]))))"
                                       title="<%=bundle.getString("birthDateT")%>"
                                       oninvalid="this.setCustomValidity(invalidInputDateFA(document.getElementById('customerBirthdateLabelTo').innerHTML))"
                                       oninput="this.setCustomValidity('')" onfocusout="this.setCustomValidity('')">
                                <%
                                } else {
                                %>
                                <input id="customerBirthdateInputTo" type="date" class="form-control"
                                       name="birthDateTo"
                                       title="<%=bundle.getString("birthDateT")%>"
                                       oninvalid="this.setCustomValidity(invalidInputDateEN(document.getElementById('customerBirthdateLabelTo').innerHTML))"
                                       oninput="this.setCustomValidity('')" onfocusout="this.setCustomValidity('')">
                                <%
                                    }
                                %>
                            </fieldset>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary"><%=bundle.getString("find")%>
                    </button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
