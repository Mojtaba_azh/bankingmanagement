<%@ page import="java.util.Locale" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    request.setCharacterEncoding("UTF-8");
    Locale persianLocale = new Locale("fa", "IR");
    Locale englishLocale = new Locale("en", "US");
    ResourceBundle bundle = null;
    String selectedLanguage = (String) request.getSession().getAttribute("selectedLanguage");
    if (selectedLanguage.equals("Persian")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/fa/MessageBundle", persianLocale);
    } else if (selectedLanguage.equals("English")) {
        bundle = ResourceBundle.getBundle("/MessageBundles/en/MessageBundle", englishLocale);
    }

    boolean customerType = (boolean) request.getAttribute("customerType");
    String jalaliBirthDate = (String) request.getAttribute("jalaliBirthDate");
    String label_customer_code;
    String label_customer_name;
    String label_customer_birthdate;
    if (customerType) {
        assert bundle != null;
        label_customer_code = bundle.getString("nationalCode");
        label_customer_name = bundle.getString("firstName");
        label_customer_birthdate = bundle.getString("birthDate");
    } else {
        assert bundle != null;
        label_customer_code = bundle.getString("economicCode");
        label_customer_name = bundle.getString("companyName");
        label_customer_birthdate = bundle.getString("registrationDate");
    }
    String textBoxLanguageClass;
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><%=bundle.getString("editCustomer")%>
    </title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link rel="stylesheet" href="css/vertical-responsive-menu.min.css"/>
    <link rel="stylesheet" href="css/persianDatepicker-default.css"/>
    <script src="js/jquery-1.10.1.min.js"></script>
    <script src="js/persianDatepicker.js"></script>
    <script src="js/functions.js"></script>
</head>
<%
    if (selectedLanguage.equals("Persian")) {
        textBoxLanguageClass = "persianTextBox";
%>
<body style="direction: rtl">
    <%
} else {
        textBoxLanguageClass="englishTextBox";
%>
<body style="direction: ltr">
<%
    }
%>
<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: #4285f4;height: 60px;">
        <ul class="navbar-nav">
            <li><a href="<%=request.getContextPath()%>/home" class="nav-link"><%=bundle.getString("homeTitle")%>
            </a></li>
            <li><a href="<%=request.getContextPath()%>/find-customer"
                   class="nav-link"><%=bundle.getString("findCustomers")%>
            </a></li>
        </ul>
    </nav>
</header>
<br>
<%
    if (selectedLanguage.equals("Persian")) {
%>
<div class="container col-md-8 text-right">
        <%
    } else {
    %>
    <div class="container col-md-8 text-left">
        <%
            }
        %>
        <div class="card">
            <div class="card-body">
                <form action="update-customer" method="post" autocomplete="off"
                        <%
                            if (selectedLanguage.equals("Persian")) {
                        %>
                      onsubmit="return validateNationalCodeForUpdateFA()"
                        <%
                        } else {
                        %>
                      onsubmit="return validateNationalCodeForUpdateEN()"
                        <%
                            }
                        %>
                >
                    <h2 class="text-center"><%=bundle.getString("editCustomer")%>
                    </h2>
                    <br><br>
                    <fieldset class="form-group">
                        <label id="customerCodeLabel"><%=label_customer_code%>
                        </label>
                        <input type="text" class="form-control numberTextBox" id="customerCodeInput" name="customerCode"
                               minlength="10" maxlength="10"
                               title="<%=label_customer_code%>" required="required"
                               value="<c:out value='${customer.customerCode}' />"
                               <%
                               if (selectedLanguage.equals("Persian")) {
                           %>oninvalid="this.setCustomValidity(invalidInputCustomerCodeFA(document.getElementById('customerCodeLabel').innerHTML))"
                            <%
                        } else {
                           %>
                               oninvalid="this.setCustomValidity(invalidInputCustomerCodeEN(document.getElementById('customerCodeLabel').innerHTML))"
                            <%
                        }
                           %>
                               oninput="this.setCustomValidity('')">
                    </fieldset>
                    <div class="row">
                        <div class="col">
                            <fieldset class="form-group">
                                <label id="customerNameLabel"><%=label_customer_name%>
                                </label>
                                <input type="text" class="form-control <%=textBoxLanguageClass%>" name="customerName"
                                       title="<%=label_customer_name%>"
                                       required="required" value="<c:out value='${customer.customerName}' />"
                                    <%
                               if (selectedLanguage.equals("Persian")) {
                           %>
                                       oninvalid="this.setCustomValidity(invalidInputFA(document.getElementById('customerNameLabel').innerHTML))"
                                    <%
                                    } else {
                                       %>
                                       oninvalid="this.setCustomValidity(invalidInputEN(document.getElementById('customerNameLabel').innerHTML))"
                                    <%
                                    }
                                       %>
                                       oninput="this.setCustomValidity('')">
                            </fieldset>
                        </div>
                        <%
                            if (customerType) {
                        %>
                        <div class="col">
                            <fieldset class="form-group">
                                <label id="customerFamilyLabel"><%=bundle.getString("lastName")%>
                                </label>
                                <input type="text" class="form-control <%=textBoxLanguageClass%>" name="customerFamily"
                                       title="<%=bundle.getString("lastName")%>"
                                       required="required" value="<c:out value='${customer.customerFamily}' />"
                                    <%
                               if (selectedLanguage.equals("Persian")) {
                           %>
                                       oninvalid="this.setCustomValidity(invalidInputFA(document.getElementById('customerFamilyLabel').innerHTML))"
                                    <%
                                    } else {
                                       %>
                                       oninvalid="this.setCustomValidity(invalidInputEN(document.getElementById('customerFamilyLabel').innerHTML))"
                                    <%
                                    }
                                        %>
                                       oninput="this.setCustomValidity('')">
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <fieldset class="form-group">
                                <label id="customerFatherLabel"><%=bundle.getString("fatherName")%>
                                </label>
                                <input type="text" class="form-control <%=textBoxLanguageClass%>" name="customerFather"
                                       title="<%=bundle.getString("fatherName")%>"
                                       required="required" value="<c:out value='${customer.customerFather}' />"
                                    <%
                               if (selectedLanguage.equals("Persian")) {
                           %>
                                       oninvalid="this.setCustomValidity(invalidInputFA(document.getElementById('customerFatherLabel').innerHTML))"
                                    <%
                                    } else {
                                       %>
                                       oninvalid="this.setCustomValidity(invalidInputEN(document.getElementById('customerFatherLabel').innerHTML))"
                                    <%
                                    }
                                        %>
                                       oninput="this.setCustomValidity('')">
                            </fieldset>
                        </div>
                        <%
                            }
                        %>
                        <div class="col">
                            <fieldset class="form-group">
                                <label id="customerBirthDateLabel"><%=label_customer_birthdate%>
                                </label>
                                <%
                                    if (selectedLanguage.equals("Persian")) {
                                %>
                                <input type="text" class="form-control birthDatePicker" name="customerBirthDate"
                                       pattern="[1-4]\d{3}\/((0?[1-6]\/((3[0-1])|([1-2][0-9])|(0?[1-9])))|((1[0-2]|(0?[7-9]))\/(30|([1-2][0-9])|(0?[1-9]))))"
                                       title="<%=label_customer_birthdate%>" required="required"
                                       value="<%=jalaliBirthDate%>"
                                       oninvalid="this.setCustomValidity(invalidInputDateFA(document.getElementById('customerBirthDateLabel').innerHTML))"
                                       oninput="this.setCustomValidity('')" onfocusout="this.setCustomValidity('')">
                                <%
                                } else {
                                %>
                                <input type="date" class="form-control" name="customerBirthDate"
                                       title="<%=label_customer_birthdate%>" required="required"
                                       value="<%=jalaliBirthDate%>"
                                       oninvalid="this.setCustomValidity(invalidInputDateEN(document.getElementById('customerBirthDateLabel').innerHTML))"
                                       oninput="this.setCustomValidity('')" onfocusout="this.setCustomValidity('')">
                                <%
                                    }
                                %>
                            </fieldset>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<c:out value='${customer.customerId}' />"/>
                    <input type="hidden" id="customerType" name="type" value="<%=customerType%>">
                    <button type="submit" class="btn btn-primary"><%=bundle.getString("save")%>
                    </button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
